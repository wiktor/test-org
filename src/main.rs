use orgize::Org;

fn main() {
    let str = std::fs::read_to_string("README.org").unwrap();
    let mut org = Org::parse_string(str);
    for node in org.arena_mut().iter_mut() {
        println!("E: {:#?}", node);
        let data = node.get_mut();
        if let orgize::Element::Title(t) = data {
            // got title
            //t.priority = Some('H');
            let url = t.properties.pairs.iter().filter(|(k,v)| k == "URL").next();
            let url: () = url;
            for (key, value) in t.properties.pairs.iter_mut() {
                if key == "NDisks" {
                    let v = (value.parse::<i32>().unwrap() + 1).to_string();
                    *value = v.into();
                }
            }
        }
        

    }

    org.write_org(std::fs::File::create("README.org").unwrap()).unwrap();
        //println!("{}", serde_json::to_string(&org).unwrap());
}

